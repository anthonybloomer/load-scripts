#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymysql.cursors
import csv
import datetime
import os

connection = pymysql.connect(host='127.0.0.1',
                             user='root',
                             password='',
                             db='smartmove',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

# Path to CSV sources
csv_dir = '/Users/anthonybloomer/ppr/csv/'

try:
    with connection.cursor() as cursor:
        for directory, subdirectories, files in os.walk(csv_dir):
            for f in files:
                print 'Inserting ' + f
                csv_data = csv.reader(file(os.path.join(directory, f)))
                csv_data.next()
                for rows in csv_data:

                    date = rows[0].split('/')
                    date = datetime.datetime(int(date[2]), int(date[1]), int(date[0]))

                    address = rows[1].lower().title()
                    postcode = rows[2]
                    county = rows[3]
                    price = rows[4][1:].replace(',', '')
                    description = rows[7]
                    cid = 1
                    sale_type = 1

                    sql = "SELECT id FROM counties WHERE county_name = %s"
                    cursor.execute(sql, county)

                    result = cursor.fetchone()

                    county_id = result['id']

                    sql = "INSERT INTO properties(date_time,address,postcode,county_id,price,description,country_id,sale_type) " \
                          "VALUES(%s, %s, %s, %s, %s, %s, %s, %s) "

                    try:
                        cursor.execute(sql, (date, address, postcode, county_id, price, description, cid, sale_type))
                        connection.commit()
                    except (pymysql.err.InternalError, pymysql.err.DataError) as e:
                        continue

    print 'Done'

finally:
    connection.close()
