# encoding=utf8

from daftlistings import Daft
import pymysql.cursors
from time import gmtime, strftime
import config

connection = pymysql.connect(host=config.host,
                             user=config.user,
                             password=config.password,
                             db=config.db,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


def exists(n):
    with connection.cursor() as cursor:
        sql = "SELECT * FROM properties WHERE address = %s"
        cursor.execute(sql, n)
        result = cursor.fetchone()
        if result:
            return True


def scrape(county_id, county, country_id):
    d = Daft()
    pages = True
    offset = 0
    print('Inserting for ' + county)

    while pages:

        listings = d.get_listings(
            county=county,
            area='',
            offset=offset,
            listing_type=config.listing_type,
            sale_type=config.sale_type,
        )

        if not listings:
            pages = False

        for listing in listings:

            address = listing.get_formalised_address()
            price = listing.get_price()

            if address is not None and price is not None:

                address = address.lower().title()

                if not exists(address):
                    with connection.cursor() as cursor:

                        print address
                        print price

                        if price == '':
                            continue

                        elif price == 'Price On Application':
                            continue

                        elif 'Reserve not to exceed' in price:
                            continue

                        elif 'In excess of' in price:
                            continue

                        elif 'AMV' in price:
                            continue

                        if country_id == 2:
                            price = price.split()
                            price = price[0]

                        l = price[:1].encode('utf-8')

                        if l == '€' or l == '£':
                            price = price[1:]
                            price = price.replace(',', '')

                        date_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
                        description = listing.get_dwelling_type()
                        rent_payment_type = None

                        if 'Per month' in price:
                            rent_payment_type = 1

                        elif 'Per week' in price:
                            rent_payment_type = 2

                        if 'Per month' or 'Per week' in price:
                            price = price.split()
                            price = price[0]

                        sql = "INSERT INTO properties (date_time, address, county_id, price, description, country_id, sale_type, rent_payment_type) " \
                              "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

                        cursor.execute(sql, (
                            date_time, address, county_id, price, description, country_id, config.sale_type_id,
                            rent_payment_type))
                        connection.commit()

        offset += 10


if __name__ == '__main__':
    with connection.cursor() as cursor:
        sql = "SELECT id, county_name, country_id FROM counties"
        cursor.execute(sql)
        results = cursor.fetchall()
        if results:
            for result in results:
                scrape(result['id'], result['county_name'], result['country_id'])
            print('Done!')
