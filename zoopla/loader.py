from zoopla import Zoopla, ZooplaException
import pymysql.cursors

zoopla = Zoopla(api_key='', debug=False, wait_on_rate_limit=True)

connection = pymysql.connect(host='127.0.0.1',
                             user='root',
                             password='',
                             db='incomestack',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


def exists(n):
    with connection.cursor() as cursor:
        sql = "SELECT * " \
              "FROM properties " \
              "WHERE address = %s"
        cursor.execute(sql, n)
        result = cursor.fetchone()
        if result:
            return True


def scrape(county, country_id):
    print('Inserting for ' + county)
    pages = True
    page_number = 1

    while pages:

        listings = zoopla.search_property_listings(params={
            'page_number': page_number,
            'page_size': 100,
            'listing_status': 'sale',
            'county': county
        })

        if not listings:
            pages = False

        for listing in listings:
            if not exists(listing.displayable_address):
                with connection.cursor() as cursor:
                    sql = "INSERT INTO properties (date_time, address, postcode, county, price, description, country_id, sale_type) " \
                          "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                    cursor.execute(sql, (listing.first_published_date, listing.displayable_address, listing.outcode, county, listing.price, listing.property_type, country_id, 2 ))
                    connection.commit()

        page_number += 1


if __name__ == '__main__':
    scrape('London', 2)
